from draughts.board import Board
from draughts.game import Game
from draughts.bot import Bot
from draughts.constants import WHITE_COUNTER, BLACK_COUNTER, BLACK
import time

def main():
    run = True
    game = Game()
    game.board.create_board()

    print("Welcome to my game of draughts (chequers). You (white pieces) play against computer (black pieces). Good luck!\n")
    
    while game.board.black_left != 0 and game.board.white_left != 0:
        game.show_stats()
        game.show_whos_turn()
        game.board.print_board()

        if game.turn == BLACK:
            print("Calcualting moves...")
            time.sleep(2)
            bot = Bot(game)
            piece_to_move, best_move_landing_position = bot.best_valid_move(game)
            source_row = piece_to_move.row
            source_col = piece_to_move.col
            destination_row, destination_col = best_move_landing_position
        else:
            source_row, source_col = game.convert_input(input("\nWhich piece you want to move (ie. '3B'): "))
            destination_row, destination_col = game.convert_input(input("Where you want to move it to (ie. '4C'): "))

        if game.is_current_players_turn(source_row, source_col):
                game.select(source_row, source_col)
                game.select(destination_row, destination_col)
        else:
                print("\nWrong piece or destination selected. Try again.\n")
                continue
        
        print('\n')

    print(f"\nGame Over.")
    game.show_stats()
        
main()