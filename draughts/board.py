from draughts.piece import Piece
from .constants import COLS, ROWS, BLACK_SQUARE, WHITE_SQUARE, LETTERS_A_H, NUMBERS_1_8, BLACK_COUNTER, WHITE_COUNTER, WHITE_KING, WHITE, BLACK


class Board:
    def __init__(self):
        self.empty_board = [[0] * COLS for i in range(ROWS)]
        self.pieces_on_board = [[0] * COLS for i in range(ROWS)]
        self.game_board = [[0] * COLS for i in range(ROWS)]
        self.selected_piece = None
        self.black_left = self.white_left = 12
        self.black_kings = self.white_kings = 0
        self._draw_squares()
        
    def _draw_squares(self):
        self.empty_board = [[BLACK_SQUARE for col in range(COLS)] for row in range(ROWS)]
        for row in range(ROWS):
            for col in range(row % 2, ROWS, 2):
                self.empty_board[row][col] = WHITE_SQUARE

    # creates a list containing representation of a game board with pieces
    def _draw_pieces(self):
        for row in range(ROWS):
            for col in range(COLS):
                if self.pieces_on_board[row][col] == 0:
                    self.game_board[row][col] = self.empty_board[row][col]
                else:
                    self.game_board[row][col] = self.pieces_on_board[row][col].figure

    def print_board(self):
        self._draw_pieces()
        print('  ' + ' '.join(LETTERS_A_H))
        itr = 0
        for i in self.game_board:
            print(NUMBERS_1_8[itr], end = ' ')
            print(' '.join(i))
            itr += 1

    # creates a list containing all pieces with their initial positions
    def create_board(self):
        for row in range(ROWS):
            #self.pieces_on_board.append([])
            for col in range(COLS):
                if col % 2 == ((row + 1) % 2):
                    if row < 3:
                        self.pieces_on_board[row][col] = Piece(row, col, BLACK, BLACK_COUNTER)
                    elif row > 4:
                        self.pieces_on_board[row][col] = Piece(row, col, WHITE,  WHITE_COUNTER)
                    else:
                        self.pieces_on_board[row][col] = 0
                else:
                    self.pieces_on_board[row][col] = 0
        self._draw_pieces()

    def get_piece(self, row, col):
        return self.pieces_on_board[row][col]

    def move(self, piece, row, col):
        self.pieces_on_board[piece.row][piece.col], self.pieces_on_board[row][col] = self.pieces_on_board[row][col], self.pieces_on_board[piece.row][piece.col]
        piece.move(row, col)

        if row == ROWS - 1 or row == 0:
            piece.make_king()
            if piece.figure == WHITE_COUNTER:
                self.white_kings += 1
            else:
                self.black_kings += 1

    def remove(self, pieces):
        for piece in pieces:
            self.pieces_on_board[piece.row][piece.col] = 0
            if piece != 0:
                if piece.color == WHITE:
                    self.white_left -= 1
                else:
                    self.black_left -= 1