from .constants import WHITE_COUNTER, WHITE_KING, BLACK_KING

class Piece:
    def __init__(self, row, col, color, figure):
        self.row = row
        self.col = col
        self.color = color # WHITE or BLACK player
        self.figure = figure # 'figure' is an ASCII char representing particulat piece (counter or king of particular color)
        self.is_king = False

    def make_king(self):
        self.is_king = True
        if self.figure == WHITE_COUNTER:
            self.figure = WHITE_KING
        else:
            self.figure = BLACK_KING

    def move(self, row, col):
        self.row = row
        self.col = col