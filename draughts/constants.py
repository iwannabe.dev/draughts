from string import ascii_uppercase

ROWS, COLS = 8, 8

WHITE = "white"
BLACK = "black"

WHITE_SQUARE = "\u2B1C" # ascii ⬜
BLACK_SQUARE = "\u2B1B" # ascii ⬛

WHITE_COUNTER = "\u263A" # ascii ☺
BLACK_COUNTER = "\u263B" # ascii ☻

WHITE_KING = "\u2654" # ascii ♔
BLACK_KING = "\u265A" # ascii ♚

LETTERS_A_H = ascii_uppercase[:8]
NUMBERS_1_8 = (range(1, 9))