from draughts.game import Game
from draughts.constants import ROWS, COLS, BLACK
import random

class Bot:
    def __init__(self, game):
        self.bot_pieces = self._bot_pieces(game)

    def _bot_pieces(self, game):
        pieces = []
        for row in range(ROWS):
            for col in range(COLS):
                if game.board.pieces_on_board[row][col] == 0:
                    continue
                elif game.board.pieces_on_board[row][col].color == BLACK:
                    pieces.append(game.board.pieces_on_board[row][col])
                else:
                    continue

        return pieces

    def best_valid_move(self, game):
        best_move_score = 0
        best_move_landing_position = []
        piece_to_move = []
        valid_moves = {}

        for piece in self.bot_pieces:
            valid_moves = game.get_valid_moves(piece)
            for landing_position, pieces_to_knock in valid_moves.items():
                if len(pieces_to_knock) >= best_move_score:
                    piece_to_move = piece
                    best_move_score = len(pieces_to_knock)
                    best_move_landing_position = landing_position

        return piece_to_move, best_move_landing_position
            
    