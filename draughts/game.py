from draughts.board import Board
from .constants import WHITE_COUNTER, BLACK_COUNTER, WHITE, BLACK, LETTERS_A_H, ROWS, COLS

class Game:
    def __init__(self):
        self.selected = None
        self.board = Board()
        self.turn = "white"
        self.valid_moves = {}

    def winner(self):
        if self.board.white_left <= 0:
            return BLACK_COUNTER
        elif self.board.black_left <= 0:
            return WHITE_COUNTER

    def change_turn(self):
        if self.turn == WHITE:
            self.turn = BLACK
        else:
            self.turn = WHITE

    def reset(self):
        self.__init__()

    def convert_input(self, user_input):
        row = int(user_input[:1]) - 1
        col = LETTERS_A_H.index(user_input[-1:].upper())

        return row, col

    def select(self, row, col):
        if self.selected:
            result = self._move(row, col)
            if not result:
                self.selected = None
                self.select(row, col)
                
        piece = self.board.get_piece(row, col)
        if piece != 0 and piece.color == self.turn:
            self.selected = piece
            self.valid_moves = self.get_valid_moves(piece)
            return True

        return False
    
    def _move(self, row, col):
        piece = self.board.get_piece(row, col)
        if self.selected and piece == 0 and (row, col) in self.valid_moves:
            self.board.move(self.selected, row, col)
            skipped = self.valid_moves[(row, col)]
            if skipped:
                self.board.remove(skipped)
            self.change_turn()
        else:
            if self.turn == WHITE:
                print("\nCan't make that move. Try again.")
            return False
        
        return True

    def get_valid_moves(self, piece):
        moves = {}
        left = piece.col - 1
        right = piece.col + 1
        row = piece.row

        if piece.color == BLACK or piece.is_king:
            moves.update(self._explore_left(row + 1, min(row + 3, ROWS), 1, piece.color, left))
            moves.update(self._explore_right(row + 1, min(row + 3, ROWS), 1, piece.color, right))

        if piece.color == WHITE or piece.is_king:
            moves.update(self._explore_left(row - 1, max(row-3, -1), -1, piece.color, left))
            moves.update(self._explore_right(row - 1, max(row-3, -1), -1, piece.color, right))

        return moves

                                       # step is direction of which we move respectively for white (down) or black (up)    
    def _explore_left(self, start, stop, step, color, left, skipped=[]):
        moves = {}
        last = []

        for r in range(start, stop, step):
            if left < 0:
                break

            current = self.board.pieces_on_board[r][left]
            if current == 0:
                if skipped and not last:
                    break
                elif skipped:
                    moves[(r, left)] = last + skipped
                else:
                    moves[(r, left)] = last

                if last:
                    if step == -1:
                        row = max(r - 3, 0)
                    else:
                        row = min(r + 3, ROWS)

                    moves.update(self._explore_left(r + step, row, step, color, left - 1, skipped = last))
                    moves.update(self._explore_right(r + step, row, step, color, left + 1, skipped = last))
                break

            elif current.color == color:
                break
            else:
                last = [current]

            left -= 1

        return moves

    def _explore_right(self, start, stop, step, color, right, skipped=[]):
        moves = {}
        last = []

        for r in range(start, stop, step):
            if right >= COLS:
                break

            current = self.board.pieces_on_board[r][right]
            if current == 0:
                if skipped and not last:
                    break
                elif skipped:
                    moves[(r, right)] = last + skipped
                else:
                    moves[(r, right)] = last

                if last:
                    if step == -1:
                        row = max(r - 3, 0)
                    else:
                        row = min(r + 3, ROWS)

                    moves.update(self._explore_left(r + step, row, step, color, right - 1, skipped = last))
                    moves.update(self._explore_right(r + step, row, step, color, right + 1, skipped = last))
                break

            elif current.color == color:
                break
            else:
                last = [current]

            right += 1

        return moves

    def display_valid_moves(self):
        temp = {}
        temp = {(str(k[0]+1) + LETTERS_A_H[k[1]]):v for k, v in self.valid_moves.items()}

        return temp
    
    def is_current_players_turn(self, row, col):
        piece = self.board.get_piece(row, col)
        if piece != 0 and piece.color == self.turn:
            return True
        else:
            return False

    def show_whos_turn(self):
        if self.turn == WHITE:
            print(f"Current player: {self.turn} {WHITE_COUNTER}\n")
        else:
            print(f"Current player: {self.turn} {BLACK_COUNTER}\n")

    def show_stats(self):
        print("Game stats:")
        print(f"White left: {self.board.white_left}")
        print(f"Black left: {self.board.black_left}\n")